Welcome to the EasyNetQ wiki

**[[EasyNetQ Documentation|Introduction]]** (English)

**[EasyNetQ Documentation](https://github.com/zanfranceschi/EasyNetQ.Docs.Portuguese/wiki)** (Portuguese)

***

**[[EasyNetQ Management API Documentation|Management-API-Introduction]]**

***

**Case study: [[RabbitMQ at 15Below]]**

***

**Roadmap: [[Roadmap]]**

***

**[[Governance principles]]**
